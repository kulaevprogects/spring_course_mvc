package com.kulaev.spring.mvc;

import com.kulaev.spring.mvc.validation.CheckEmail;

import javax.validation.constraints.*;
import java.util.HashMap;
import java.util.Map;

public class Employee {
    @Size(min = 2,message = "name must be minimum 2 symbols")
    private String name;
//    @NotEmpty(message = "surname is required field")
    @NotBlank(message = "surname is required field")
    private String surname;
    @Min(value = 501, message = "salary must be greater than 500")
    @Max(value = 999, message = "salary must be less than 1000")
    private int Salary;
    private String department;
    private  String carBrand;
    private Map <String,String> departments;
    private Map <String,String> carBrands;
    private String[] languages;
    private Map <String,String> languageList;
    @Pattern(regexp = "\\d{3}-\\d{2}-\\d{2}", message = "please use pattern XXX-XX-XX")
    private String phoneNumber;
    @CheckEmail(value = "abc.com", message = "email must ends with abc.com")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Map<String, String> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(Map<String, String> languageList) {
        this.languageList = languageList;
    }

    public Map<String, String> getDepartments() {
        return departments;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public Map<String, String> getCarBrands() {
        return carBrands;
    }

    public void setCarBrands(Map<String, String> carBrands) {
        this.carBrands = carBrands;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public void setDepartments(Map<String, String> departments) {
        this.departments = departments;
    }
    public Employee() {
        this.departments = new HashMap<>();
        this.departments.put("Information Technologies","IT");
        this.departments.put("Human Resources","HR");
        this.departments.put("Philosophy","Philosophy");
        this.carBrands = new HashMap<>();
        this.carBrands.put("BMW","BMW");
        this.carBrands.put("Audi","Audi");
        this.carBrands.put("Niva","Niva");
        this.languageList = new HashMap<>();
        this.languageList.put("English","EN");
        this.languageList.put("Deutch","DE");
        this.languageList.put("French","FR");
    }
    public Employee(String name, String surname, int salary) {
        this.name = name;
        this.surname = surname;
        Salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSalary() {
        return Salary;
    }

    public void setSalary(int salary) {
        Salary = salary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", Salary=" + Salary +
                ", departments=" + departments +
                '}';
    }
}
