package com.kulaev.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/employee")
public class MyController {
    @RequestMapping("/")
    public String showFirsrView(){
        return "firstView";
    }

    @RequestMapping("/askDetails")
    public String askEmployeeDetails(Model model){
//        Employee employee = new Employee("Ivan","Ivamov",300);
        Employee employee = new Employee();
//        System.out.println(employee);
        model.addAttribute("employee", employee);
        return "ask-employee-details-view";
    }

    @RequestMapping("/showDetails")
    public String showEmployeeDetails(@Valid @ModelAttribute("employee") Employee employee, BindingResult bindingResult){
//        System.out.println("surname length ="+employee.getSurname().length());
        if(bindingResult.hasErrors()){
            return "ask-employee-details-view";
        }
        return "show-employee-details-view";
    }



//
//    @RequestMapping("/showDetails")
//    public String showEmployeeDetails(@ModelAttribute("employee") Employee employee){
//        String name = employee.getName();
//        name = "Mr. "+ name;
//        employee.setName(name);
//        String surname = employee.getSurname();
//        employee.setSurname(surname+"!!!");
//        int salary = employee.getSalary();
//        salary = salary*10;
//        employee.setSalary(salary);
//        return "show-employee-details-view";
//    }
//    @RequestMapping("/askDetails")
//    public String askEmployeeDetails(){
//        return "ask-employee-details-view";
//    }
//    @RequestMapping("/showDetails")
//    public String showEmployeeDetails(){
//        return "show-employee-details-view";
//    }
//    @RequestMapping("/showDetails")
//    public String showEmployeeDetails(HttpServletRequest request, Model model){
//        String employeeName = request.getParameter("employeeName");
//        employeeName = "Mr." + employeeName;
//        model.addAttribute("employeeAttribute",employeeName);
//        model.addAttribute("description"," programmist ");
//
//        return "show-employee-details-view";
//    }
//     @RequestMapping("/showDetails")
//     public String showEmployeeDetails(@RequestParam("employeeName") String name, Model model){
//         name = "Mr." + name +"!";
//         model.addAttribute("employeeAttribute",name);
//         model.addAttribute("description","  programmist ");
//         return "show-employee-details-view";
//     }
}
