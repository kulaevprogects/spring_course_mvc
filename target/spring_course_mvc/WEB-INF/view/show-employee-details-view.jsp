<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<body>
<h2>Dear employee!!!!You are welcome!!!</h2>
<br>
<br>
<br>
<%--Your name: ${param.employeeName}--%>
Your name:${employee.name}
<br>
<br>
Your surname:${employee.surname}
<br>
<br>
Your salary:${employee.salary}
<br>
<br>
Your department:${employee.department}
<br>
<br>
Your car:${employee.carBrand}
<br>
<br>
Language(s)
<UL>
    <c:forEach var="lang" items="${employee.languages}">
    <li>${lang}</li>
    </c:forEach>
</UL>
<br>
<br>
Your phoneNumber:${employee.phoneNumber}
<br>
<br>
Your email:${employee.email}
</form>
</body>
</html>